package hybrid;

import api.Souvenir;
import api.Tour;

public class QuickTourImpl implements Tour {

    @Override
    public String name() {
        return "Short tour";
    }

    @Override
    public int length() {
        return 30;
    }

    @Override
    public Souvenir getSouvenir() {
        Souvenir gift = new Souvenir();
        gift.setDescription("keychain");
        return gift;
    }
}

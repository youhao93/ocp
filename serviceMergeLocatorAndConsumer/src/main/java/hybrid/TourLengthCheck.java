package hybrid;

import api.Tour;

import java.util.OptionalInt;
import java.util.ServiceLoader;
import java.util.ServiceLoader.Provider;

public class TourLengthCheck {
    public static void main(String[] args) {
        OptionalInt serviceLoader = ServiceLoader.load(Tour.class)
                .stream()
                .map(Provider::get)
                .mapToInt(Tour::length)
                .max();
        System.out.println(serviceLoader);

        OptionalInt min = ServiceLoader.load(Tour.class)
                .stream()
                .map(Provider::get)
                .mapToInt(Tour::length)
                .min();
        System.out.println(min);
    }
}

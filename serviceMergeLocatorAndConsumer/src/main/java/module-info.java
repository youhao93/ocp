module serviceMergeLocatorAndConsumer {
    requires serviceProviderInterfaceApi;
    provides api.Tour with hybrid.QuickTourImpl;
    uses api.Tour;
}
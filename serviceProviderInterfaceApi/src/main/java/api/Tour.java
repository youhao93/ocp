package api;

public interface Tour {
    String name();
    int length();
    Souvenir getSouvenir();
}

module serviceLocatorReservation {
    exports reservation;

    //requires de hele module
    requires serviceProviderInterfaceApi;

    //requires de specifieke klasse van de module
    uses api.Tour;
}
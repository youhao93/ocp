
package Multithreading;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

public class ThreadSafe {
    private AtomicInteger integer = new AtomicInteger(10);
    private int x = 10;
    private static int y = 10;
    final Object obj = new Object();

    public static void main(String[] args) throws Exception {
        ExecutorService executorService = null;

        try {
            executorService = Executors.newFixedThreadPool(10);
            ThreadSafe threadSafe = new ThreadSafe();
            for (int i = 0; i < 10; i++) {
                //executorService.submit(threadSafe::incrementAndReportSynchronized2);
                ThreadSafe.incrementAndReportSynchronizedStatic();
            }

        } finally {
            if (executorService != null) executorService.shutdown();
        }
    }

    public void incrementAndReport() {
        System.out.print(integer.incrementAndGet() + " ");
    }

    public void incrementAndReportNotSafe() {
        System.out.print(++x + " ");
    }

    public void incrementAndReportSynchronized1() {
        synchronized (obj) {
            System.out.print(++x + " ");
        }
    }

    public synchronized void incrementAndReportSynchronized2() {
        System.out.print(++x + " ");
    }

    public static void incrementAndReportSynchronizedStatic() {
        synchronized (ThreadSafe.class) {
            System.out.print(++y + " ");
        }
    }


}

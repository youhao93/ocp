package Multithreading;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.stream.Collectors;

public class SkipListCollections {
    public static void main(String[] args) {
        /**
         * Bij een SkipListMap kan je niet direct stream van maken. Wel bij een Set.
         * Je moet bij een map eerst entrySet() methode gebruiken.
         * Bij een Set gebruik je de add() methode. Bij een Map gebruik je de
         * put() methode
         */
        Set<String> skipListSet = new ConcurrentSkipListSet<>();
        skipListSet.add("Hoi");
        skipListSet.add("Hoi1");
        skipListSet.add("Hoi2");
        System.out.println(skipListSet.stream()
                .collect(Collectors.joining(",")));

        Map<String, String> skipListMap = new ConcurrentSkipListMap<>();
        skipListMap.put("Key1", "Waarde1");
        skipListMap.put("Key2", "Waarde2");
        skipListMap.put("Key3", "Waarde3");
        skipListMap.entrySet().stream().forEach(System.out::println);
    }
}

package Multithreading;

public class MultiThreading {

    public static void main(String[] args) throws InterruptedException {
        System.out.println("Aantal available processors " + Runtime.getRuntime().availableProcessors());
        //task
        Runnable snake = () -> System.out.println("Dit is een snake");
        //run task
        snake.run();

        //print data implements runnable, dus moet je zelf een thread aanmaken
        //om te kunnen starten
        new Thread(new PrintData()).start();
        Thread.sleep(10000);
        //deze extends Thread, dus kan je direct start op aanroepen
        new ReadInventoryThread().start();

        //je kan het in een thread zetten, maar start methode doet het al voor je
        new Thread(new ReadInventoryThread()).start();
    }
}

class PrintData implements Runnable {
    @Override
    public void run() {
        for(int i = 0; i < 2; i++) {
            System.out.println("Multithreading.PrintData " + i);
        }
    }
}

class ReadInventoryThread extends Thread {
    @Override
    public void run() {
        for(int i = 0; i < 2; i++) {
            System.out.println("Multithreading.ReadInventoryThread " + i);
        }
    }
}
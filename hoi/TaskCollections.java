package Multithreading;


import java.util.List;
import java.util.concurrent.*;

public class TaskCollections {
    public static void main(String[] args) throws Exception {
        ExecutorService service = Executors.newSingleThreadExecutor();
        ExecutorService serviceCachedThreadPool = Executors.newCachedThreadPool();
        ExecutorService serviceFixedThreadPool = Executors.newFixedThreadPool(10);
        ScheduledExecutorService scheduledService = Executors.newSingleThreadScheduledExecutor();

        Callable<String> c1 = () -> "Een string";
        Callable<String> c2 = () -> "Een string c2";

        serviceFixedThreadPool.submit(c1);

        try {
            List<Future<String>> listCallables = service.invokeAll(List.of(c1, c2, c1));

            for(Future<String> f : listCallables) {
                System.out.println(f.get());

            }
        } finally {
            service.shutdown();
        }

    }

}

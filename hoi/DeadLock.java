package Multithreading;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class Food{}
class Water{}

public class DeadLock {

    public void eatDrink(Food food, Water water) {
        synchronized (food) {
            System.out.println("Got food");
            move();
            /**
             * Tot hier en niet verder
             * water object is nog in lock door methode beneden
             */
            synchronized (water) {
                System.out.println("Got water");
            }
        }
    }

    public void drinkEat(Food food, Water water) {
        synchronized (water) {
            System.out.println("Got water");
            move();
            /**
             * Tot hier en niet verder
             * food object is nog in lock door methode hierboven
             */
            synchronized (food) {
                System.out.println("Got food");
            }
        }
    }

    public void move() {
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public static void main(String[] args) {
        ExecutorService service = null;
        Food food = new Food();
        Water water = new Water();
        DeadLock dl1 = new DeadLock();
        DeadLock dl2 = new DeadLock();

        try {
            service = Executors.newFixedThreadPool(10);
            service.submit(() -> dl1.eatDrink(food, water));
            service.submit(() -> dl2.drinkEat(food, water));
        } finally {
            assert service != null;
            service.shutdown();
        }

    }


}



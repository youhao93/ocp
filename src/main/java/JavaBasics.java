enum OnlyOne {
    ONCE(true);
    private OnlyOne(boolean b) {
        System.out.println("Constructing,");
    }
}

public class JavaBasics {
    private String greeting = "Hi";

    public class Inner {
        public int repeat = 3;
        public void go() {
            for(int i = 0; i < repeat; i++) {
                System.out.println(greeting);
            }
        }
    }

    public void callInner() {
        Inner inner = new Inner();
        inner.go();
    }
    public static void main(String[] args) {

        JavaBasics javaBasics = new JavaBasics();
        javaBasics.callInner();
        /*
        Je mag geen instantie maken van een inner class
         */
        //Inner inner = new Inner();
        Inner inner = javaBasics.new Inner();
        inner.go();

        System.out.println("Begin,");
        OnlyOne firstCall = OnlyOne.ONCE;
        //constructor doesn't get called anymore after once
        OnlyOne secondCall = OnlyOne.ONCE;
        System.out.println("End");
        Fox.Nested.print();
        Fox fox = new Fox();
        fox.accommpanyFox();
        fox.visitFox();
    }
}

class Fox {

    public int age = 0;
    private int width = 20;
    public String name = "Foxie";
    public static int year = 15;
    /*
    Bij static klassen kan je niet zomaar
    instance variabelen benaderen uit de
    enclosing class. Je hebt daarvoor eerst een instance nodig van
    de klasse zelf
     */
    static class Nested{
        public static void print() {
            int x = new Fox().age;
            System.out.println(x);
            System.out.println(year);
            Fox fox = new Fox();
            fox.visitFox();
        }
    }

    interface SaleTodayOnly {
        void dollarsoff();
    }

    public void visitFox() {

        /*
        Anonymous klasse kan je ook als een parameter
        in een method schrijven
         */
        SaleTodayOnly sale = new SaleTodayOnly() {
            private int test1 = 0;
            /*
            Bij interface klasse moet je public voor de method zetten
            Bij abstract class hoef je geen public ervoor te zetten
             */
            @Override
            public void dollarsoff() {
                System.out.println("Ik word vanuit een anonymous class aangeroepen");
            }
        };

        sale.dollarsoff();

        //static class niet toegestaan en deze klasse bestaat
        // pas als de methode visitFox() aangeroepen wordt
        class FoxLocal {
            //static fields niet toegestaan
//            public static int x2 = 0;

            //static final fields wel toegestaan
            public static final int x = 0;
            Fox fox = new Fox();
            int age = fox.age;

            //static methodes niet toegestaan
//            public static void foxLocalReturn() {
//
//            }
        }

    }
    /*
    class -> method -> local class -> method of local class
     */
    public void accommpanyFox() {
        final int length = 20;
        //moet final of effectively final zijn
        final int horizontal = 20;
        class MyLocalClass {
            int height = 100;
            public void multiply() {
                System.out.println(width * length * height * horizontal);
            }
        }
        MyLocalClass local = new MyLocalClass();
        local.multiply();
        /*dit is niet toegestaan. Als het effectively final is, mag de
        waarde na initialisatie niet opnieuw gedeclareerd worden. Met final
        keyword, compileert de code niet eens
         */
        //horizontal = 10;
    }


}
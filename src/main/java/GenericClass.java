


import java.util.*;
import java.util.stream.Collectors;

public class GenericClass implements Comparable<GenericClass>, Comparator<GenericClass> {

    private int num;
    private String value;

    public GenericClass(int num, String value) {
        this.num = num;
        this.value=value;
    }
    public static <T> T ship(T t) {
        return t;
    }
    public String toString() {
        return "" + num;
    }

    @Override
    public int compareTo(GenericClass s) {
        return s.getValue().compareTo(this.value);
    }

    @Override
    public int compare(GenericClass u, GenericClass t) {
        return u.getNum() - t.getNum();
    }

    public int getNum() {
        return num;
    }
    public String getValue() {
        return value;
    }
    public static <U extends Exception> String printException(U u) {
        System.out.println(u.getMessage());

        return "";
    }

    public static void main(String[] args) {
        GenericClass s1 = new GenericClass(88, "a");
        GenericClass s2 = new GenericClass(55, "b");

        TreeSet<GenericClass> t1 = new TreeSet<GenericClass>();
        t1.add(s1);
        t1.add(s2);

        List<GenericClass> list1 = new ArrayList<>();
        list1.add(s1);
        list1.add(s2);
        Collections.sort(list1);
        System.out.println("List1: " + list1);
        Collections.sort(list1, (x,y) -> x.compareTo(y));

        List<Integer> integers = new ArrayList<>();
        integers.add(1);
        integers.add(2);
        List<? extends Number> numbers = integers;
        System.out.println("number specific: " + numbers.get(0));
        //bij extends kan niks toevoegen, alleen bij super numbers.add(1);
        System.out.println("integers: " + integers);
        System.out.println("numbers: " + numbers);
        /*
        Wat hier gebeurt is dat een object van GenerClassQuestions in TreeSet wordt meegegeven.
        TreeSet constructor heeft een comparator als parameter type. Die comparator kijkt naar method override
        compare. Die is geimplementeerd op basis van ascending order. Dus de t2 TreeSet wordt gesorteerd o.b.v.
        de body in compare method, wat de nummers zijn. Vandaar dat dus [55, 88] wordt teruggegeven.

        Als er geen GenerClassQuestions object was meegegeven, dan was de compareTo methode genomen die naar de text
        van het object kijkt.
         */
        TreeSet<GenericClass> t2 = new TreeSet<GenericClass>(s1);
        t2.add(s1);
        t2.add(s2);

        System.out.println(t1 + " " + t2);

        Map m = new HashMap();
        m.put(123, "Test");
        m.put("abc", 13);
        System.out.println(m.containsKey("123"));
        System.out.println(m);

        var map = Map.of(1,2,3,6);
        var list = List.copyOf(map.entrySet());
        System.out.println("List with map entrySet");
        System.out.println(list.get(0));
        System.out.println(list);
        List<Integer> one = List.of(8, 16, 2);
        var copy = List.copyOf(one);
        var copyOfCopy = List.copyOf(copy);
        var thirdCopy = new ArrayList<>(copyOfCopy);
        var fourth = new ArrayList<>(copyOfCopy);

        //geeft compilatie error op EntrySet
//        list.replaceAll(x -> x*2);
//        one.replaceAll(x -> x * 2);
        thirdCopy.replaceAll(x -> x * 2);
        System.out.println(map);

        var hashmap = new HashMap<Integer, Integer>();
        hashmap.put(1, 10);
        hashmap.put(2, 20);
        hashmap.put(4, 20);
        hashmap.put(3, null);
        hashmap.merge(1, 5, (a, b) -> a+b);
        System.out.println(hashmap);
        //method reference
        hashmap.merge(3, 3, Integer::sum); //null bij key 3 wordt vervangen door waarde 3
        System.out.println("Hashmap met merge " + hashmap);

        Set<Map.Entry<Integer, Integer>> entrySet = hashmap.entrySet();
        System.out.println(entrySet);
        System.out.println(entrySet.stream().peek(System.out::println));
        System.out.println(hashmap.keySet());
        Collection<Integer> values = hashmap.values();
        System.out.println(values);


    }
}

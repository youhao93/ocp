package EnumsExample;

import java.util.Arrays;

public class EnumsExample {

    public enum Season {
        ONCE(true) {
            public String getHours() {
                return "10am till 10pm";
            }
        }, TWICE(true) {
            @Override
            public String getHours() {
                return "5 tot 15";
            }

            public int gettime() {
                return 10;
            }
        };

        /**
         * Default implementatie
         * @return String tijd
         */
        public String getHours() {
            return "10 tot 12";
        };

        private Season(boolean b) {
            System.out.println("Enum constructor gets called");
        }


    }
    public static void main(String[] args) {
        Season firstCall = Season.ONCE;
        Season secondCall = Season.TWICE;
        Season thirdCall = Season.TWICE;

    }
}


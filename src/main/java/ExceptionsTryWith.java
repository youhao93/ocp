import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class ExceptionsTryWith {
    public static void main(String[] args) {
        //Dolphin dolphin = new Dolphin();
        UsingAutoCloseableType usingAutoCloseableType = new UsingAutoCloseableType();
        try {
            usingAutoCloseableType.tryWithMethod();
            usingAutoCloseableType.tryWithMethod2();
            usingAutoCloseableType.tryWithMethod3();
//            dolphin.swim();
//        } catch(CannotSwimException e) {
//            System.out.println(e.getMessage());
        } catch (Exception e) {
            System.out.println("Exception message: " + e.getMessage());
        }
        String x = "Een naampje";
        assert 1 == 5: "Probleem met iets";
        assert("Kees".equals(x)) : "Toch niet gelijk aan elkaar";

        LocalDate date = LocalDate.parse("2021-12-15", DateTimeFormatter.ISO_LOCAL_DATE_TIME);
        System.out.println(date.getYear() + " " + date.getMonth() + " " + date.getDayOfMonth() + " Testttt");

    }

    public static void ietsMetExceptions() throws SQLException {
        try(var in = new FileInputStream("data.txt");
           var out = new FileOutputStream("output.txt")) {
            System.out.println("Try clause is uitgevoerd");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Hoi");
        }
    }
}


class CannotSwimException extends Exception {

    public CannotSwimException() {
        super(); //dit is optioneel, compiler doet dit automatisch voor je
    }

    public CannotSwimException(Exception e) {
        super(e);
    }

    public CannotSwimException(String message) {
        super(message);
    }
}
class dangerInWaterException extends RuntimeException {
}
class SharkInWaterException extends dangerInWaterException {}
class Dolphin {
    public void swim() throws CannotSwimException {
//        try {
            System.out.println("Swim or else an Cannot swim exception");
            throw new CannotSwimException("Dit is een exception cannotSwim bericht");
            //compile time error, want CannotSwimException wordt nooit gegooid in de try block
//        } catch(CannotSwimException e) {
//            e.getMessage();
//        }
    }
}

/*
Automating resource management
Je moet een type hebben die Autocloseable interface implementeert
close() method is overridden
type gebruiken in parameter
 */

class ResourceReader implements AutoCloseable {
    private String text;

    public ResourceReader(String text) {
        this.text = text;
    }
    @Override public void close() {
        System.out.println("Type object is " + text + ", is dichtgemaakt");
    }

}

class MyFileReader implements AutoCloseable {

    @Override public void close () {
        System.out.println("Throwed exception");
        throw new IllegalStateException("MyFileReader could not be closed");
    }

    public String printMessage () {
        return "Printing stuff from MyFileReader obj";
    }

    /*
    Hier komt suppressed exceptions in voor. Als zowel in try clause en in close() method exceptions worden opgegooid,
    dan pakt java de eerste thrown exception in the try clause. The close() method wordt daarna automatisch door
    Java uitgevoerd die ook een exception opgooid. Deze exception heet dan de suppressed exception, omdat java die wel
    onthoudt en onder de primary exception uitprint. In de catch clause wordt vervolgens alleen gecheckt of die primary
    exception matched wat in de catch clause exception staat. Zoniet, dan gaat Java verder kijken naar de main method waar
    zowel de primary als de suppressed exceptions worden uitgeprint. Suppressed exceptions zijn alleen de exceptions die in
    de try clause worden opgegooid. Exceptions die daarna in bijv. de finally block worden opgegooid, tellen niet.
     */
    public static void main(String[] args) {
        try(MyFileReader m = new MyFileReader()) { //suppressed exception
            System.out.println(m.printMessage());
            throw new IllegalStateException("First exception"); //primary exception
        } catch (IllegalStateException e) { //exception moet matchen met primary exception, zo niet, dan door naar main
            for(Throwable th : e.getSuppressed()){ //loop over alle suppressed exceptions die Java onthoudt
                System.out.println("Supressed: " + th.getMessage());
            }
        }
    }
}

class UsingAutoCloseableType {

    public void tryWithMethod() {
        try(var x = new ResourceReader("ResourceReader")) {
            System.out.println("Try statement");
        } finally {
            System.out.println("Finally block executes");
        }
    }

    public void tryWithMethod2() {
        final var y = new ResourceReader("ResourceReaderY");
        var z = new ResourceReader("ResourceReaderZ");
        try(var x = new ResourceReader("ResourceReader");
            y;
            z) {
            System.out.println("Try statement");
        } finally {
            System.out.println("Finally block executes");
        }
        //z=new ResourceReader("Dit werkt niet, niet effectively final");
    }

    public void tryWithMethod3() throws IOException{
        var writer = Files.newBufferedWriter(Path.of(String.valueOf(Path.of("test"))));
        writer.append("Dit mag je doen, maar wordt afgeraden");
        try(writer) {
            System.out.println("tryWithMethod3");
            writer.append("Welcome to you too");
        }
        //writer.append("Dit mag dus niet, geeft RuntimeException (Stream closed");
    }
}

/*
Voorbeeldcode dat dit niet compileert

3: try (Scanner s = new Scanner(System.in)) {
4:    s.nextLine();
5: } catch(Exception e) {
6:    s.nextInt(); // DOES NOT COMPILE
7: } finally {
8:    s.nextInt(); // DOES NOT COMPILE
9: }

Dit compileert niet omdat Scanner s alleen in de try scope
beschikbaar is en als de try block eenmaal is is uitgevoerd,
dan verdwijnen de resources ook. Deze resources zijn dan al gesloten
 */

class Danger extends Exception {
    public Danger() {
        super();
    }

}

class Emergency extends Danger {
}
import java.text.NumberFormat;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Properties;

import static java.time.format.FormatStyle.*;

public class Local {
    public static void main(String[] args) {
        Locale locale = Locale.UK;
        System.out.println(locale);
        System.out.println("language and country");
        /*
        Let op de output of the locales in de juiste format
        is geschreven.
         */
        System.out.println(Locale.GERMANY);
        System.out.println(Locale.US);
        System.out.println(new Locale("fr", "FR"));
        System.out.println(new Locale("fr"));
        Locale l1 = new Locale.Builder()
                .setRegion("HH")
                .setLanguage("us")
                .build();
        Locale l2 = new Locale.Builder()
                .setRegion("CA")
                .setLanguage("fr")
                .build();
        System.out.println(l1);
        System.out.println(l2);
        System.out.println(NumberFormat.getCurrencyInstance());
        System.out.println(NumberFormat.getInstance(l1));

        int attendeesPerYear = 3_200_000;
        int attendeesPerMonth = attendeesPerYear / 12;

        var us = NumberFormat.getCurrencyInstance(Locale.US);
        System.out.println(us.format(attendeesPerMonth));
        System.out.println(us);

        var gr = NumberFormat.getCurrencyInstance(Locale.GERMANY);
        System.out.println(gr.format(attendeesPerMonth));
        System.out.println(gr);

        var ca = NumberFormat.getCurrencyInstance(Locale.CANADA_FRENCH);
        System.out.println(ca.format(attendeesPerMonth));
        System.out.println(ca);

        var uk = NumberFormat.getCurrencyInstance(Locale.UK);
        System.out.println(uk.format(attendeesPerMonth));

        try {
            String income = "$92807.99";
            System.out.println("test");
            var cf = NumberFormat.getCurrencyInstance(Locale.US);
            double value = (Double) cf.parse(income);
            System.out.println(value); // 92807.99
        }
        catch(ParseException e) {
            System.out.println(e);
        }

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME;
        LocalDateTime dateTime = LocalDateTime.now();
        System.out.println(dateTimeFormatter.format(dateTime));
        System.out.println(dateTimeFormatter.withLocale(locale).format(dateTime));

        Locale.setDefault(new Locale("us", "US"));
        var italy = new Locale("it", "IT");
        var dt = LocalDateTime.of(2021, 12, 29, 16, 35, 55);
        print(DateTimeFormatter.ofLocalizedDate(SHORT), dt, italy);
        print(DateTimeFormatter.ofLocalizedDate(LONG), dt, italy);
        System.out.println("Test");
        print(DateTimeFormatter.ofLocalizedTime(SHORT), dt, italy);
        print(DateTimeFormatter.ofLocalizedDateTime(SHORT, SHORT), dt, italy);

        var spain = new Locale("es", "ES");
        var money = 1.23;
        printCurrency(spain, money);
        Locale.setDefault(Locale.Category.DISPLAY, spain);
        printCurrency(spain, money);
        Locale.setDefault(Locale.Category.FORMAT, spain);
        printCurrency(spain, money);
        Properties properties = new Properties();
        properties.setProperty("Hoi", "Hallo");
        System.out.println(properties);
        System.out.println(properties.getProperty("Hoi", "Dit is de default value als er geen value bestaat"));
    }
    public static void print(DateTimeFormatter df, LocalDateTime dateTime, Locale locale) {
        System.out.println(df.format(dateTime));
        System.out.println(df.withLocale(locale).format(dateTime));
    }

    public static void printCurrency(Locale locale, double money) {
        System.out.println(NumberFormat.getCurrencyInstance().format(money) + ", " + locale.getDisplayLanguage());
    }

}

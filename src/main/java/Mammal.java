


public class Mammal {
    public int getAverageSpeed() {
        return 5;
    }
}

interface CanRun {
    public int getRunningSpeed();
}

class Zebra extends Mammal implements CanRun {
    public int getRunningSpeed() {
        return 40;
    }

    public static void main(String[] args) {
        Zebra zebra = new Zebra();
        CanRun runner = zebra;
        Mammal mammel = zebra;

        System.out.println(mammel.getAverageSpeed());
        System.out.println(zebra.getAverageSpeed());
        System.out.println(runner.getRunningSpeed());
    }
}



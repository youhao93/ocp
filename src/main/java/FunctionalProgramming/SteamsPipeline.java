package FunctionalProgramming;

import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

public class SteamsPipeline {

    Stream<Integer> emptyStream = Stream.empty();
    Stream<String> singleValue = Stream.of("String waarde", "Tweede waarde");
    Stream<Integer> fromArray = Stream.of(1, 2, 3);
    List<String> list = List.of("1", "2", "3", "test", "test1", "test2", "test3");
    List<Integer> listInt = List.of(1, 2, 3, 4, 5);

    //waardes in de stream worden gelijktijdig opgepakt en uitgeprint
    Stream<String> fromList = list.parallelStream();

    String r1 = list.stream().reduce("", (x, y) -> x + y);
    int r2 = listInt.stream().reduce(0, Integer::sum);

    public static void main(String[] args) {
        SteamsPipeline sp = new SteamsPipeline();
        System.out.println("r1: " + sp.r1);
        System.out.println("r1: " + sp.r2);
        System.out.println("Hello world");

        SteamsPipeline s1 = new SteamsPipeline();
        s1.emptyStream.forEach(System.out::println);
        s1.singleValue.forEach(System.out::println);

        s1.fromList.forEach(System.out::println);

        Stream<Integer> oddNumber = Stream.iterate(
                1,
                n -> n <100,
                n -> n + 2
        );
        System.out.println("resultaat oddNumber");
        oddNumber.forEach(System.out::print);
        System.out.println();

        Stream<String> stream = Stream.of("w", "o", "l", "f!");
        //w + 0 = 2
        //l + f! = 3
        //combiner doet 2 + 3
        /*
        die stream referentie is van type String. Je kan methode string length
         op roepen om gaandeweg het totaal te weten. Maar de identifier is een int
         en wat de terug verwacht is ook een int, alleen is de stream een String.
         Door de combiner wordt de uiteindelijke waarde een int.
         Als het type van de stream niet gelijk is aan de variable type, dan moet een
         combiner gebruikt worden die alles bij elkaar optelt en een int van maakt
         */
//        int length = stream.reduce(0, (x, y) -> x + y.length(), (a, b) -> a + b);

        //System.out.println(length);

//        StringBuilder stringBuilder = stream.collect(
//                StringBuilder::new,
//                StringBuilder::append,
//                StringBuilder::append);
//        System.out.println(stringBuilder);

//        TreeSet treeSet = stream.collect(
//                TreeSet::new,
//                TreeSet::add,
//                TreeSet::addAll);
//        System.out.println(treeSet);

//        Kortere manier
//        TreeSet<String> treeSet = stream.collect(Collectors.toCollection(TreeSet::new));
//        Set<String> treeSet2 = stream.collect(Collectors.toSet());
//        System.out.println(treeSet2);

        Stream<String> filterStream = Stream.of("Monkey", "Gorilla", "Bonobo", "Bonobo", "Bonobo", "Bonobo");
        filterStream.distinct()
                .forEach(e -> System.out.println("Distinct: " + e));
        System.out.println("Test");
        Stream<Integer> integerList = Stream.iterate(1, n -> n + 2);
        integerList.skip(5)
                    .limit(2)
                    .forEach(System.out::print);
        System.out.println();

        List<String> empty = List.of();
        var less = List.of("Gorilla", "Ape");
        var full = List.of("Gorilla", "Ape", "Monkey", "I am a big boy", "I am a big boy", "I am a big boy");
        Stream<List<String>> streamOfLists = Stream.of(empty, less, full);
        streamOfLists.flatMap(Collection::stream).forEach(System.out::println);
     // streamOfLists.flatMap(e -> e.stream()).forEach(System.out::println); Same


        Stream<Integer> numbers = Stream.of(1, 2, 3, 4, 5);
        numbers.map(e -> e + 2)
                //.flatMap(e -> e.stream()) // Dit is dus niks. Map is element voor element.
                                            // flatMap neemt een collectie en maakt daar een eigen stream van per element
                .forEach(System.out::println);
//
//        animals.map(String::length) //mapped elk element tot een nieuw element in een nieuwe stream
//                .filter(e -> e > 10)
//                .distinct()
//                .forEach(System.out::println);

    }

}

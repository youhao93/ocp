package FunctionalProgramming;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class CollectorsExample {

    public static void main(String[] args) {
//        Stream<String> streams = Stream.of("Bear", "Dolphin", "Turtle", "Panda", "Walrus");
//        Map<Integer, List<String>> tm = streams.collect(Collectors.groupingBy(
//                        e -> e.length(),
//                        Collectors.toList()));
//        System.out.println(tm);
//
//        Stream<String> streams2 = Stream.of("Bear", "Dolphin", "Turtle", "Panda", "Walrus");
//        TreeMap<String, Long> tm2 = streams2.collect(Collectors.groupingBy(
//                e -> e,
//                TreeMap::new,
//                Collectors.counting()));
//        System.out.println(tm2);
//
//        Stream<String> streams3 = Stream.of("Bear", "Dolphin", "Turtle", "Panda", "Walrus", "Bear");
//        Map<String, List<String>> tm3 = streams3.collect(Collectors.groupingBy(
//                e -> e,
//                Collectors.toList()));
//        System.out.println(tm3);

//        Stream<String> streams4 = Stream.of("Bear", "Dolphin", "Turtle");
//        streams4.map(e -> e + "hallo").forEach(System.out::println);

        Stream<String> streams3 = Stream.of("Bear", "Dolphin", "Turtle", "Panda", "Walrus", "Bear");
        Map<Integer, String> tm3 = streams3.collect(Collectors.toMap(
                String::length,
                e -> e,
                (x,y) -> x + "," + y));

        System.out.println(tm3);
        String string = "Hbllo";
        System.out.println(string.compareTo("Hbi"));

        Stream<Integer> intStream = Stream.of(1,2,3,4);
        IntStream is = intStream.mapToInt(e -> e);
        is.forEach(System.out::print);
        System.out.println();
        IntStream.range(1,6).forEach(System.out::print);

    }
}

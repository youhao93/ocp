package FunctionalProgramming;

import java.util.*;
import java.util.function.*;
import java.util.stream.*;

import static java.util.stream.Collectors.*;

public class PrimitiveStreams {

    public static void main(String[] args) {
        //partitionThings();
        List<Person> x = List.of(
                new Person("Lary"),
                new Person("Kim"),
                new Person("Piet"),
                new Person("John"),
                new Person("Jon"),
                new Person("Jon"),
                new Person("Jon"));

//        List<String> list = List.of("abcd", "abg", "abigail", "abg", "abg");
//        var xx1 = list.stream();
//        System.out.println();
//        xx1.map(e -> new StringBuilder(e).reverse()).forEach(System.out::println);

//        TreeMap<Integer, String> map = list.stream().collect(Collectors.toMap(
//                String::length,
//                e -> e,
//                (s1,s2) -> s1 + "," + s2,
//                TreeMap::new));
//        System.out.println(map);

        Map<String, List<Person>> map1 = x.stream().collect(Collectors.groupingBy(e -> e.getName()));
        Map<String, List<Integer>> map2 = x.stream().collect(Collectors.groupingBy(Person::getName, Collectors.mapping(e -> e.name.length(), Collectors.toList())));
        Map<String, List<Long>> map3 = x.stream().collect(Collectors.groupingBy(Person::getName, mapping(e -> (long)e.name.length(), toList())));

        Stream<Person> s1 = x.stream();
        Stream<Person> s2 = s1.filter(e -> e.getName().length() > 3);
        Stream<Person> s3 = s2.peek(System.out::println);
        Map<String, List<Person>> s4 = s3.collect(Collectors.groupingBy(e -> e.getName()));
        System.out.println(s4);

        System.out.println("Maps");
        System.out.println(map1);
        System.out.println(map2);
        System.out.println(map3);



    }

    static <X> void fifth(List<? super String> list) {
    Integer xx = 15_000_000;
        System.out.println("Int number: "  + xx);
    }



    private static void partitionThings() {
        /*Deze stream blijft leeg, zolang er geen terminal operation bij zit. Omdat de stream
        nooit wordt aangeroepen, wordt de referentie naar deze stream teruggegeven
         */

        System.out.println(Stream.iterate(1, x -> ++x)
                .peek(System.out::print)
                .limit(5).map(x -> "" + x)
                .collect(Collectors.joining()));

        Predicate<String> predicate = s -> s.length() > 3;
        var stream1 = IntStream.of(8, 10, 12);
        double result = stream1.mapToLong(x -> x)
                .boxed()
                .collect(Collectors.groupingBy(x -> x))
                .keySet()
                .stream()
                .collect(Collectors.averagingDouble(x -> x));
        System.out.println(result);

        var stream = Stream.iterate("", (s) -> s + "1");
        System.out.println("Test " + stream.limit(2).map(x -> x + "2").collect(Collectors.joining()));

        Stream<String> x1 = Stream.of("Dolphin", "Gorilla", "Tiger", "Mammal");
        Map<Boolean, List<String>> partition = x1
                .peek(System.out::println)
                .sorted()
                .filter(e -> e.length() > 3)
                .collect(Collectors.partitioningBy(s -> s.length() > 5));
        // andere return value
//         Map<Boolean, Set<String>> partition = x1.collect(Collectors.partitioningBy(s -> s.length() > 5, Collectors.toSet()));
        System.out.println(partition);

        // Moeilijkste code
        /*
        Laten we zeggen dat we de eerste letter van een dier alfabetisch willen van elke lengte.
         */
        Stream<String> x2 = Stream.of("Dolphin", "Gorilla", "Tiger", "Mammal");
        Map<Integer, Optional<Character>> partition2 = x2.collect(
                Collectors.groupingBy(
                        String::length,
                        mapping(
                                s -> s.charAt(0),
                                Collectors.minBy((a,b) -> a - b)
                )));
        System.out.println("Partition2: "  + partition2);

    }




    private static void collectingThings() {

        //De return value in map aanpassen naar Set
        var x6 = Stream.of("Lions", "Tigers", "Dolhpin1", "Bears", "Dolhpin2");
        // de return waarde een Set van maken door de static methode in Collectors toSet aan te roepen
        TreeMap<Integer, Set<String>> map4 = x6.collect(Collectors.groupingBy(
                String::length,
                TreeMap::new,
                toSet()));

//        Je kan ook het aantal dieren tellen die dezelfde string length hebben
        /*
        Map<Integer, Long> map4 = x6.collect(Collectors.groupingBy(
                String::length,
                Collectors.counting()));
         */
        System.out.println(map4);


        //De return value in map aanpassen naar Set
        Stream<String> x5 = Stream.of("Lions", "Tigers", "Dolhpin1", "Bears", "Dolhpin2");
        // de return waarde een Set van maken door de static methode in Collectors toSet aan te roepen
        Map<Integer, Set<String>> map3 = x5.collect(Collectors.groupingBy(String::length, toSet()));
        System.out.println(map3);



        var x4 = Stream.of("Lions", "Tigers", "Dolhpin1", "Bears", "Dolhpin2");
        //de groupingBy return geen null keys
        Map<Integer, List<String>> map2 = x4.collect(Collectors.groupingBy(String::length));
        System.out.println(map2);
        var x3 = Stream.of("Lions", "Tigers", "Dolhpin", "Bears");
        TreeMap<Integer, String> map1 = x3.collect(Collectors.toMap(
                String::length, // key
                s-> s, // value
                (s1, s2) -> s1 +"hoii" + "," + s2, //wat moet de mapper doen als er twee gelijke key's zijn
                TreeMap::new)); // deze laatste parameter is voor constructor class die aangemaakt wordt
        System.out.println(map1); // {5=Lionshoii,Bears, 6=Tigers, 7=Dolhpin}
        System.out.println(map1.getClass()); // TreeMap

        var x2 = Stream.of("Lions", "Tigers", "Dolhpin");
        Map<String, Integer> map = x2.collect(Collectors.toMap(s -> s, String::length));
        System.out.println(map);
        var x = Stream.of("Lions", "Gorilla" , "Sea fish");
        Double s1 = x.collect(Collectors.averagingDouble(String::length));
        System.out.println(s1);

        var x1 = Stream.of("Lions", "Tigers", "Dolhpin");
        TreeSet<String> t1 = x1.filter(s -> s.startsWith("T"))
                .collect(Collectors.toCollection(TreeSet::new));
        System.out.println(t1);




    }

    private static void functionalInterfacePrimitives() {
        //SUPPLIER
        //implementeer de supplier middels lambda
        DoubleSupplier doubleX = () -> 12.0;
        //Daarna kan je een get doen om, in dit geval, de double eruit te halen
        double herro = doubleX.getAsDouble();
        System.out.println(herro);

        BooleanSupplier booleanX = () -> true;
        boolean booleanValue = booleanX.getAsBoolean();
        System.out.println(booleanValue);

        //CONSUMER
        DoubleConsumer consumer1 = (double x) -> System.out.println(x);
        consumer1.accept(10.0);

        //PREDICATE
        IntPredicate p1 = (int x) -> x > Math.PI;
        boolean value = p1.test(20);
        System.out.println("Bigger or smaller than PI: " + value);

        //FUNCTION
        // er gaat een referentie type in
        IntFunction<Integer> xFunction = (int x) -> x + 10;
        //accepteerd primitieve (int) waarde
        int valueFunction = xFunction.apply(10);
        System.out.println(valueFunction);

        /* Unary operator: lijkt op function, alleen met Unary operator weet
        je precies welk primitive type je terug krijgt in de diamond operator. I
         */
        DoubleUnaryOperator doubleU = (double doubleArgument) -> doubleArgument + 10.0;
        double resultDouble = doubleU.applyAsDouble(10.0);
        System.out.println(resultDouble);

        ToDoubleFunction<String> toDoubleFunction = String::length;
        double waardeToDouble = toDoubleFunction.applyAsDouble("Halloooooo");
        System.out.println(waardeToDouble);
        Optional<String> optional = Optional.of("Halloo");
        //Optional<Integer> value1 = optional.map(ChainingOptionals::calculator);
        /*
        Als ChainingOptionals een Optional<Integer> teruggeeft, dan werkt dat niet, want de type variabele
        verwacht gewoon een Integer. Anders krijg je: Optional<Optional<Integer>>.
        Wat je kan doen is die return waarde, Optional<Integer>, flatMappen, zodat je dan gewoon netjes
        de waarde uit de Optional krijgt. Dus een Integer. Dan past die wel in die type variabele.
         */
    }


    private static void flatMap() {
        var numbers = new ArrayList<Integer>();
        numbers.add(1);
        numbers.add(3);
        numbers.add(5);
        DoubleStream primitiveStream = numbers.stream().flatMapToDouble(x -> DoubleStream.of(x));
        primitiveStream.forEach(System.out::print);
        var stream = LongStream.of(1, 10, 5);
        LongSummaryStatistics x = stream.summaryStatistics();
        System.out.println(x.getMax() - x.getMin());
    }


    private static void createStream() {
        Stream<Integer> numbers = Stream.of(1, 2, 3, 4, 5);
        IntStream intStream = numbers.mapToInt(e -> e);

        IntStream intStream2 = IntStream.of(1, 2, 3, 4);

        Stream<Integer> stream = intStream2.mapToObj(e->e);
        stream.forEach(System.out::println);
    }
}

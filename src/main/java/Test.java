import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Test {

    public static void main(String[] args) {
        String[] array = { "hawk", "dove" };
        List<String> list2 = Arrays.asList(array);	// From array to List results in a “Backed list”,
        // the array and the list remain linked
        System.out.println(list2.set(0, "Hi"));
        System.out.println(list2);			// [hawk, dove]
        System.out.println(Arrays.toString(array));			// [hawk, dove]

        Period period= Period.of(10, 10,10);

        LocalDate date1 = LocalDate.of(2018, Month.AUGUST, 6);
        LocalDateTime date2 = LocalDateTime.of(2018, Month.AUGUST, 6, 15, 12, 30);

        DateTimeFormatter dtf = DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT);
        DateTimeFormatter dtf2 = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT);

        System.out.println(dtf.format(date1));
        System.out.println(dtf2.format(date2));




    }
}

package NIO2;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.*;

public class FilesExample {
    public static void main(String[] args) throws IOException {

//        System.out.println(Files.copy(Path.of("examCopy.txt"), Path.of("nieuwe file"),
//                StandardCopyOption.REPLACE_EXISTING));
//        System.out.println(Files.copy(Path.of("pandas"), Path.of("pandas2")));
//        Files.copy(Paths.get("examCopy.txt"), System.out);
//        try(var is = new FileInputStream("examCopy.txt")) {
//            Files.copy(is, Paths.get("examCopy2.txt"));
//        }

        var path1 = Path.of("examCopy.txt");
        var directory = Path.of("pandas/examCopy.txt");
        Files.copy(path1, directory);

    }
}

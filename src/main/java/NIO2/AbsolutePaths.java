package NIO2;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class AbsolutePaths {
    public static void main(String[] args) throws IOException {
        Path path1 = Path.of("/pony/../weather.txt");
        Path path2 = Path.of("weather.txt"); // check wel of dit om een relative path gaat
                                                  // of om een file/directory
        System.out.println(path1.equals(path2));
        System.out.println(path1.normalize());
        System.out.println(path2.normalize());
        System.out.println(path1.normalize().equals(path2.normalize()));

        System.out.println(Paths.get("./test").toRealPath());



    }
}

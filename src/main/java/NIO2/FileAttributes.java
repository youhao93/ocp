package NIO2;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;

public class FileAttributes {
    public static void main(String[] args) throws IOException {
        var path = Paths.get("examCopy.txt");
        BasicFileAttributes data = Files.readAttributes(path, BasicFileAttributes.class);
        System.out.println(data.isDirectory());
        System.out.println(data.isRegularFile());
        System.out.println(data.lastModifiedTime());

        BasicFileAttributeView view = Files.getFileAttributeView(path, BasicFileAttributeView.class);
        BasicFileAttributes readData = view.readAttributes();
        System.out.println(readData.isDirectory());

        FileTime lastModified = FileTime.fromMillis(readData.lastModifiedTime().toMillis() + 10000);
        view.setTimes(lastModified, null, null);

    }
}

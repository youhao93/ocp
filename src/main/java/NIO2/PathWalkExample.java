package NIO2;

import java.io.IOException;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Stream;

public class PathWalkExample {

    public static void main(String[] args) throws IOException {
        PathWalkExample pwe = new PathWalkExample();
        System.out.println(pwe.getTotalPathSize(Path.of("src/main/java/IO")));
    }

    private long getSize(Path p) {
        long size = 0;
        try {
            size = Files.size(p);
        } catch(IOException e) {
            e.printStackTrace();
        }
        return size;
    }

    public long getTotalPathSize(Path p) throws IOException {
        try(Stream<Path> sp = Files.walk(p, 10, FileVisitOption.FOLLOW_LINKS)) {
            return sp.parallel()
                    .filter(e -> !Files.isDirectory(e))
                    .mapToLong(this::getSize)
                    .sum();
        }
    }
}

package NIO2;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Stream;

public class PathFunctionProgramming {
    public static void main(String[] args) throws IOException {
        Path path = Path.of("src/main/java/Multithreading/");
        Path target = Path.of("hoi");
        try (Stream<Path> sp = Files.list(path)) {
            sp.forEach(System.out::println);
        }
        PathFunctionProgramming pfp = new PathFunctionProgramming();
        pfp.copyPath(path, target);
    }

    public void copyPath(Path source, Path target) {
        try {
            Files.copy(source, target);
            if (Files.isDirectory(source)) {
                try (Stream<Path> sp = Files.list(source)) {
                    /**
                     * Recursive call die telkens Files.copy opnieuw uitvoert
                     */
                    sp.forEach(e -> copyPath(e,
                            target.resolve(e.getFileName())));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

package NIO2;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.sql.SQLOutput;
import java.util.stream.Stream;

public class FindFiles {
    public static void main(String[] args) {
        Path p = Path.of("src/main/java/NIO2");
        try(Stream<Path> files = Files.find(p,  10, (a,b) ->
                a.toString().endsWith(".java")
                && b.isRegularFile()
                && b.size() >1000)) {
            {
            files.forEach(System.out::println);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}

package NIO2;

import java.nio.file.Path;
import java.nio.file.Paths;

public class Paths2Example {
    public static void main(String[] args) {

        Path path = Path.of("/pandas/sanctionary/panda.txt");
        Path parent = path.getParent();
        System.out.println(parent);
        System.out.println(path.getName(0));
        System.out.println(path.getNameCount());

        Path path2 = Path.of("zoo");
        Path path3 = Path.of("/pandas/sanctionary/panda.txt");
        Path path4 = Path.of("./pandas/../sanctionary/panda.txt");
        Paths2Example paths2Example = new Paths2Example();

//        paths2Example.printPathInformation(path2); //null, null, zoo
//        paths2Example.printPathInformation(path3); //panda.txt, /, /pandas/sanctionary, /pandas, ./
//        paths2Example.printPathInformation(path4); //panda.txt, /, ./pandas/../sanctionary, ./pandas/.., ./pandas, ./


        var path5 = Paths.get("/pandas/Hello.txt");
        var path6 = Paths.get("zoo/birds.txt");
        System.out.println("Is absolute path5? " + path5.isAbsolute());
        System.out.println("Is absolute path6? " + path6.isAbsolute());

        System.out.println("Is absolute path5? " + path5.toAbsolutePath());
        System.out.println("Is absolute path6? " + path6.toAbsolutePath());
    }

    public void printPathInformation(Path path) {
        System.out.println("Printing file name " + path.getFileName());
        System.out.println("Printing root name " + path.getRoot());
        Path currentPath = path;
        while((currentPath = currentPath.getParent()) != null) {
            System.out.println("Parent is " + currentPath);
        }

    }
}

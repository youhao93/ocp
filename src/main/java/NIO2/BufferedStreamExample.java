package NIO2;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class BufferedStreamExample {
    public static void main(String[] args) throws IOException {
        Path path1 = Path.of("examCopy.txt");
        try(var bufferedReader = Files.newBufferedReader(path1)) {
            String x = null;
            while((x = bufferedReader.readLine()) !=null) {
                System.out.println(x);
            }
        }

        ArrayList<String> list = new ArrayList<>();
        list.add("Hoi");
        list.add("iedereen");

        Path path2 = Path.of("examCopy3.txt");
        try(var bufferedWriter = Files.newBufferedWriter(path2)) {
            for(String s : list) {
                bufferedWriter.write(s);
                bufferedWriter.newLine();
            }
        }

        Path p3 = Path.of("examCopy3.txt");
        Files.deleteIfExists(p3);

        Files.createDirectories(Path.of("hoi/hoi1/hoi2"));
        System.out.println();
        Path p4 = Path.of("examCopy.txt");
        List<String> list2 = Files.readAllLines(p4);
        list2.forEach(System.out::println);



    }
}

package NIO2;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Stream;

public class readingLinesPath {
    public static void main(String[] args) throws IOException {
        Path path = Path.of("src/main/java/NIO2/AbsolutePaths.java");
        Path path2 = Path.of("src/main/java/NIO2" , "AbsolutePaths.java");
        final Path path3 = Path.of(".").normalize();

//        try(Stream<String> sp = Files.lines(path)) {
//            sp.map(e -> e + "Halloo")
//                    .forEach(System.out::println);
//        }
        System.out.println(path3.toAbsolutePath());
    }
}

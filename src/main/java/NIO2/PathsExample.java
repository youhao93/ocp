package NIO2;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class PathsExample {
    public static void main(String[] args) throws URISyntaxException {
        //Path Path.of();
        Path path1 = Path.of("/pandas/Hello.txt");
        Path path2 = Path.of("/pandas/Hello2.txt");
        Path path3 = Path.of("/", "home", "zoodirectory");
        System.out.println(path1);
        System.out.println(path2);
        System.out.println(path3);

        //Path Paths.get();
        Path path4 = Paths.get("/test/test1.txt");
        Path path5 = Paths.get("/pandas/test2.txt");
        System.out.println(path4);
        System.out.println(path5);

        //URI
//        URI a = new URI("file://icecream.txt");
//        Path b = Path.of(a);
//        URI c = b.toUri();
//        System.out.println(b);
//        System.out.println(c);


    }


}

package Security;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class LeakingResource {
    public static void main(String[] args) throws IOException {
        Path path = Path.of("src/main/java/NIO2/AbsolutePaths.java");
        System.out.println(countLines(path));
    }

    public static long countLines(Path path) throws IOException {
        try(var stream = Files.lines(path)) {
            return stream.count();
        }
    }
}

package Security;

import java.util.ArrayList;

public class CloneExample implements Cloneable {
    ArrayList<String> list;

    public CloneExample(ArrayList<String> list) {
        this.list = (ArrayList) list.clone();
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public static void main(String[] args) throws CloneNotSupportedException {
        ArrayList<String> food = new ArrayList<>();
        food.add("Grass");
        CloneExample se = new CloneExample(food);
        CloneExample clone = (CloneExample) se.clone();
        System.out.println(se == clone);
        System.out.println(se.list == clone.list);
        se.list.add("Meat");
        clone.list.add("Meat");

        System.out.println(se.list);
        System.out.println(clone.list);
    }


}

package ClassesExample;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;

class Booby {
}

class Dooby extends Booby {
}

class Tooby extends Dooby {
}

public class TestClass {
    Booby b = new Booby();
    Tooby t = new Tooby();

    public void do1(List<? super Dooby> dataList) {
        //1 INSERT CODE HERE
        dataList.add(t);
        dataList.add(new Dooby());
        Object obj = dataList.get(0);

        /*
        Bij super is de flexibiliteit dat je van alles kan toevoegen, want het ondersteunt alle
        data types van X tot aan Object, alleen is het nadeel dat je niks kan ophalen, tenzij je
        Object als referentie gebruikt.
        Als je iets toevoegt moet het van type super X zijn of een subklasse van X, want alle elementen moeten
        nog wel aan X toegewezen kunnen worden.
         */
    }

    public void do2(List<? extends Dooby> dataList) {
        //2 INSERT CODE HERE
        b = dataList.get(0);
        Object obj = dataList.get(0);

        /*
        Bij extends kan je niks toevoegen, want het zijn alleen (sub)klassen onder Dooby en je weet dan niet wat
        de subklassen zijn van Dooby. Is niet bekend. Wat je wel kan doen zijn objecten ophalen, omdat een
        bovengrens voor type is aangegeven, namelijk Dooby of hoger.
         */
    }
}
/*
b=dataList.get(0);
t=dataList.get(0);
dataList.add(b);
dataList.add(t);

 */
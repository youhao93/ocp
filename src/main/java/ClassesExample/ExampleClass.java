package ClassesExample;

import java.util.Locale;
import java.util.ResourceBundle;

public class ExampleClass {
    int y = 10;
    static int x = 200;

    protected class InnerClass {
        public int x = 10;
        //shadowing y value from outer class
        private int y = 100;

        static final int b = 10;

        public void changeValue() {
            x = x -5;
        }


        public Animal getValue() {

            System.out.println(new ExampleClass().y);;
            final int localVariable = 10;
            final int localVariable2 = 20;
            final int localVariable3 = 30;

            class InsideMethod {
                public void printNumbers() {
                    System.out.println("Printing local variables: " + localVariable + localVariable2 + localVariable3);
                }
            }

            InsideMethod im = new InsideMethod();
            im.printNumbers();

            return new Animal() {

                private int localVariable = 0;
                public int localVariable2 = 0;
                int localVariable3 = 0;
                @Override
                void getSomeValue() {
                    System.out.println("Printing abstract method inside abstract class Animal");
                    int i = 1234567890;
                    float f = i;
                    System.out.println(i - (int)f);
                    Locale locale = new Locale("fr", "FR");
                    ResourceBundle rb = ResourceBundle.getBundle("appmessages", Locale.CHINA);
                    class TestClass {
                        public static final int hoi = 0;
                        public void printTestClassMessage() {
                            System.out.println("Printing a message inside a local class in a override method");
                        }
                    }

                    abstract class LocalAbstractClass {
                        String x = "Hoi";
                        String y = "Hoi";
                        public void printSomething() {

                        }
                    }

                    TestClass tc = new TestClass();
                    tc.printTestClassMessage();
                }
            };
        }
    }

    protected static class StaticClass {
        private static int x = 500;
        static void printValue() {
            System.out.println("Waarde van x " + ExampleClass.x);
        }
        void changeValue() {
            x = 100;
        }
    }

    abstract static class StaticClassAbstract {

    }

    public ExampleClass() {

    }
    public void createInner() {
        InnerClass ic = new InnerClass();
        System.out.println(ic.toString());
    }

    public void printExampleClass() {
        System.out.println("Example class");
    }
    public static void main(String[] args) {
        ExampleClass ec = new ExampleClass();
        InnerClass ic = ec.new InnerClass();
        System.out.println(ic.x);
        ic.changeValue();
        Animal animal = ic.getValue();
        animal.getSomeValue();
        StaticClass sc = new StaticClass();
        sc.changeValue();
        StaticClass.printValue();
        System.out.println(StaticClass.x);

    }


}

abstract class Animal {
    int animalInt = 5;
    abstract void getSomeValue();
}
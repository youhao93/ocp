package NewBeginnings;

public class Fox {
    private int butter = 5;
    protected class Movie {
        public static final int butter = 10;
        public Movie() {

        }
        void printValue() {
            System.out.println(butter);
        }
    }

    public static void main(String[] args) {
        Fox.Movie movie = new Fox().new Movie();
        movie.printValue();
    }
}

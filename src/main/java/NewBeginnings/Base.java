package NewBeginnings;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Base {
    public String x = "20 string";
    int y = 100;
    private static final String countBase = "Hallo";
    static {
        System.out.println("Hallo: "+ countBase);
    }
    {
        var x = 100;
        System.out.println(x);
    }
    public Base() {
        this(10);
        var x = 100;
        System.out.println("xxxxxxx");
    }

    public Base(int x) {

    }

    protected final static void hallo() {
        System.out.println("Hallo static final method");
    }

    @Pandas(hoursPerMonth=10, strings={"String1"})
    @Lions({})
    @Lions({})
    public void getValue() throws IOException {
        System.out.println("Some value from Base class");
        System.out.println("This line gets called");
    }




}

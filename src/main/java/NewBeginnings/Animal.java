package NewBeginnings;

import java.util.List;

public interface Animal {

    static private void aMethod() {

    }

    abstract String getName();
    abstract String climb();
    abstract String hunt();

    private void roar() { getName(); climb(); hunt();}
}

package NewBeginnings;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

@Target({ElementType.METHOD})
public @interface LionsList {
    Lions[] value();
}

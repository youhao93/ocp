package NewBeginnings;

import static java.lang.Boolean.TRUE;

public class Person {
     enum hoi {
          MAMMAL(TRUE), BIRD(Boolean.TRUE), OWL(true) {
               void aMethod() {
                    System.out.println("Nothing");
               }
          };
          final Boolean hasHair;
          private hoi(Boolean hasHair) {
               this.hasHair = hasHair;
          }
     }
     private int count;
     private interface Wild{}

     static class Den {}

     public static void main(String[] args) {
          Person.Den den = new Person.Den();

          System.out.println(den.getClass());
     }
     protected static class Foxie implements Wild {
          public void staticMethod() {

          }
     }
}

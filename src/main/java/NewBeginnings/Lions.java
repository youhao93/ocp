package NewBeginnings;

import java.lang.annotation.ElementType;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Target;

@Repeatable(LionsList.class)
@Target({ElementType.METHOD})
@interface Lions {

    public abstract String test() default "hoi";
    String[] value();
    int temperature() default 10;

}

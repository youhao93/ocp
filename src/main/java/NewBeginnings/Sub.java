package NewBeginnings;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Sub extends Base{
    private int countBase = 20;
    public int x = 10;
    public void getValue() throws IOException {
        super.getValue();
        System.out.println(countBase);
        System.out.println("Some value from Sub class");
    }


    public void getValue(String value)  {
        System.out.println("Some value from Sub class with extra value");
    }

    public void getValue(ArrayList<Double> value)  {
        System.out.println("Some value from Sub class with extra value");
    }


}

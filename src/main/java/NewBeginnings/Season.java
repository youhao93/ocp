package NewBeginnings;

public enum Season {
    SUMMER("very hot") {
        public void getValue() {
            System.out.println("Very hot");
        }
    }, WINTER("very cold"){
        public void getValue() {
            System.out.println("Very hot");
        }
    };
    private Season(String description) {
        System.out.println("Constructor gets called");
    }

    public abstract void getValue();
}

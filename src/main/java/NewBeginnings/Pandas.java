package NewBeginnings;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

@Target({ElementType.TYPE, ElementType.METHOD})
public @interface Pandas {
    int hoursPerDay() default 10;
    int hoursPerMonth();
    String someLiteral() default "Hallo";
    String[] strings();
    int[] numbers() default {1, 2, 3};
}

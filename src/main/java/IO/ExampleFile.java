package IO;

import java.io.File;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

public class ExampleFile {

    public static void main(String[] args) {
        System.out.println(File.separator);
        System.out.println(System.getProperty("file.separator"));
        System.out.println("File roots: " + Arrays.toString(File.listRoots()));

        var file = new File("consumerVisitor");
        var file2 = new File("src/main/java/IO/ExampleFile.java");

        File parent = new File("src/main/java");
        File fullPath = new File(parent, "IO/ExampleFile.java");
        System.out.println(fullPath.exists());

        if(fullPath.exists()) {
            System.out.println("Absolute path: " + file.getAbsolutePath());
            System.out.println("Is Directory: " + file.isDirectory());
            System.out.println("Is file: " + file.isFile());
            System.out.println("Is parent path: " + file2.getParent());
            if(fullPath.isFile()) {
                System.out.println("Size: "  + fullPath.length());
                System.out.println("Last modified: " + fullPath.lastModified());
            } else {
                for (File subfile : fullPath.listFiles()) {
                    System.out.println(" " + subfile.getName());
                }
            }

            Charset usAsciiCharset = StandardCharsets.US_ASCII;
            System.out.println(usAsciiCharset);
        }

    }
}

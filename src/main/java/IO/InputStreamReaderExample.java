package IO;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class InputStreamReaderExample {

    public static void main(String[] args) throws IOException {
        var reader = new BufferedReader(new InputStreamReader(System.in));
        String userInput = reader.readLine();
        System.out.println("Input: " + userInput);


        /**
         * Die hele System outputstream wordt gesloten. Kan dan nergens meer in de
         * applicatie die System.out nog gebruiken
         */
//        try(var out = System.out) {}
//        System.out.println("Hello");

//        var reader2 = new BufferedReader(new InputStreamReader(System.in));
//        try(reader2) {}
//        String userinput = reader2.readLine(); //dit geeft error,
        // want reader2 is al closed door try with clause



    }
}

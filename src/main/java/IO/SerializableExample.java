package IO;

import java.io.*;
import java.util.ArrayList;

public class SerializableExample {


    public static void main(String[] args) throws IOException {

        ArrayList<Tail> tails = new ArrayList<>();
        Tail tail1 = new Tail();
        Tail tail2 = new Tail();
        tails.add(tail1);
        tails.add(tail2);

        try (FileOutputStream fos = new FileOutputStream("tailss");
             ObjectOutputStream objectOutputStream = new ObjectOutputStream(fos)) {
            for(Tail tail : tails) {
                objectOutputStream.writeObject(tail);
            }
        }
        readObject();
    }

    private static void readObject() throws IOException {
        try (FileInputStream fis = new FileInputStream("tailss");
             ObjectInputStream objectInputStream = new ObjectInputStream(fis)) {
            while(true) {
                Tail tail = (Tail) objectInputStream.readObject();
                System.out.println(tail);
            }
        } catch (ClassNotFoundException | EOFException e) {
            e.printStackTrace();
        }

    }

}

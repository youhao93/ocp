package IO;

import java.io.Serializable;

public class Tail implements Serializable {
    String name;
    transient int age;
    /**
     * Tail wordt meegenomen van memory naar file, maar niet Fox, want transient. Wordt
     * dus geskipt. Klasse Fox implementeert ook niet Serializable, dus krijg je
     * "NotSerializableException"
     */
    private transient Fox fox = new Fox();
    public Tail() {
        name = "Tailssss";
        age = 10;
    }

    public String toString() {
        return name + " " + age + "";
    }
}

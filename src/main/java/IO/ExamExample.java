package IO;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class ExamExample {
    public static void main(String[] args) throws IOException {
        File file = new File("log.txt");
        File file2 = new File("examCopy.txt");
        ExamExample examExample = new ExamExample();
        examExample.copyFile(file, file2);
    }

    public void copyFile(File file1, File file2) throws IOException {
        var reader = new InputStreamReader(new FileInputStream(file1));
        try(var writer = new FileWriter(file2); reader) {
            char[] buffer = new char[10];
            while(reader.read(buffer) != -1) {
                writer.write(buffer);
            }
        }
        ConcurrentMap<Integer, String> m1 = new ConcurrentHashMap<>();
        Map<String, Integer> m2 = new HashMap<>();
    }
}

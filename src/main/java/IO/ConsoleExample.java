package IO;

import java.io.Console;

public class ConsoleExample {
    public static void main(String[] args) {

        Console console = System.console();

        System.out.println(console);
        if(console!=null) {
            console.writer().println("Er is een console ");
        } else {
            System.err.println("No console present");
        }
    }
}

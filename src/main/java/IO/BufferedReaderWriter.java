package IO;

import java.io.*;

public class BufferedReaderWriter implements Serializable {
    /**
     * Tail wordt meegenomen van object in memory naar file
     */
    private Tail tail = new Tail();

    public static void main(String[] args) throws IOException {
        File file = new File("original.txt");
        File file2 = new File("copy.txt");
        writeFile(file);
        copyFile(file, file2);
        readFile(file2);
    }

    public static void writeFile(File src) throws IOException{
        try(var out = new BufferedWriter(new FileWriter(src))) {
            for(int i = 0; i <5; i++) {
                out.write("Hello world " + i);
                out.newLine();
            }
        }
    }
    public static void copyFile(File src, File dest) throws IOException {
        try (var in = new BufferedReader(new FileReader(src));
             var out = new BufferedWriter(new FileWriter(dest))) {
            String b;
            /**
             * read() returnt int waarde terug
             * readLine() returnt String waarde terug
             */
            while((b = in.readLine()) !=null) {
                /**
                 * newLine om nieuwe regel toe te voegen
                 */
                String c = b.toUpperCase();
                out.write(c);
                out.newLine();
                out.flush();
            }
        }
    }

    public static void readFile(File file) throws IOException {
        try(var bufferedReader = new BufferedReader(new FileReader(file))) {
            String s;
            while ((s = bufferedReader.readLine())!=null) {
                System.out.println(s);
            }
        }
    }
}

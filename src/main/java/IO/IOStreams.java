package IO;

import java.io.*;

public class IOStreams {
    /**
     * read() (InputStream) returnt int waarde terug en checkt op -1
     * readLine() (Reader) returnt String waarde terug en checkt op null


     * FileOutputStream
     * FileInputStream
     * <p>
     * BufferedWriter --> Writer
     * BufferedReader --> Reader
     * <p>
     * PrintStream --> System.out / System.err
     * PrintWriter
     */
    public static void main(String[] args) throws IOException {
        File file = new File("text.txt");
        try (var out = new BufferedWriter(new FileWriter(file))) {
            for (int i = 0; i < 5; i++) {
                out.write("Hello world " + i + "\n");
                out.flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        readFile("text.txt");
        File file2 = new File("text2.txt");
        copyFile(file, file2);
    }

    public static void readFile(String fileName) throws IOException {
        try (var bf = new BufferedReader(new FileReader(fileName))) {
            String s;
            while ((s = bf.readLine()) != null) {
                System.out.println(s);
            }
        }
    }

    public static void copyFile(File src, File dest) throws IOException {
        try (var in = new BufferedInputStream(new FileInputStream(src));
             var out = new BufferedOutputStream(new FileOutputStream(dest))) {
            int b;
            /**
             * read() (InputStream) returnt int waarde terug en checkt op -1
             * readLine() (Reader) returnt String waarde terug en checkt op null
             */
            var buffer = new byte[1024];
            while((b = in.read(buffer)) !=-1) {
                /**
                 * Als je write(buffer, offset, value) methode gebruikt,
                 * dan moet je bij in.read() ook een buffer aangeven, dus in.read(buffer)
                 */
                out.write(buffer, 0, b);
                out.flush();
            }
        }
    }
}

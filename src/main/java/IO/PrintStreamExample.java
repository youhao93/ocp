package IO;

import java.io.*;

public class PrintStreamExample {
    public static void main(String[] args) throws IOException {
        File fileZoo = new File("Zoo.txt");
        PrintStream printStream = new PrintStream(fileZoo);

        File file = new File("log.txt");
        Tail tail = new Tail();
        try(var out = new PrintWriter(
                new BufferedWriter(
                        new FileWriter(file)))) {
            out.print("Hallo daar");
            out.println();
            out.println("Dit is een voorbeeldzin");
            out.format("Naam %s met leeftijd %d en gewicht %.1f", "Jan", 25, 75.105);
            out.println();
            out.println(tail);
        }
    }
}

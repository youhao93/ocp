import java.io.FileNotFoundException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;

public abstract class Oefenexamen1 implements TriFunction {

    public static void main(String[] args) {


        List<String> vowels = new ArrayList<>();
        vowels.add("a");
        vowels.add("e");
        vowels.add("i");
        vowels.add("o");
        vowels.add("u");
        Function<List<String>, List<String>> f = list -> list.subList(2, 4);
        vowels = f.apply(vowels);
        vowels.forEach(System.out::print);
        System.out.println();
        double number = 1000.0;
        int x = 10, y = 20;
        int dx, dy;
        try {
            dx = x % 5;
            dy = y / dx;
        } catch (ArithmeticException ae) {
            System.out.println("Caught AE");
            dx = 2;
            dy = y / dx;
        }
        x = x / dx;
        y = y / dy;
        System.out.println("Hallo");
        System.out.println(dx + " " + dy);
        System.out.println(x + " " + y);
    }

    abstract public void add();
}

abstract class AmazingClass {
    void amazingMethod(Collection c) {
        System.out.println("Hallo");
    }
}


class StringArrayTest {
    public static void main(String args[]) {
        String[][][] arr = {{{"a", "b", "c"}, {"d", "e", null}}, {{"x"}, null}, {{"y"}}, {{"z", "p"}, {}}};
        System.out.println(arr[0][1][2]);
    }
}
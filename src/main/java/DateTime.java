import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class DateTime {
    public static void main(String[] args) {
        System.out.println(LocalDate.now());
        System.out.println(LocalDateTime.now());
        System.out.println(ZonedDateTime.now());
        LocalDateTime x = LocalDateTime.of(2021, 12, 5, 12, 30, 55);
//        System.out.println(x.getDayOfMonth());
//        System.out.println(x.getDayOfWeek());
//        System.out.println(x.getDayOfMonth());
//        System.out.println(x.getMonth());
//        System.out.println(x.getDayOfYear());
        LocalDate y = LocalDate.of(2021, 12, 5);
        LocalTime z = LocalTime.of(15, 22, 33);
        LocalDateTime yz = LocalDateTime.of(y,z);
        System.out.println(yz.format(DateTimeFormatter.BASIC_ISO_DATE));
        System.out.println(yz.format(DateTimeFormatter.ISO_DATE));
        System.out.println(yz.format(DateTimeFormatter.ISO_LOCAL_DATE));
        var formatter = DateTimeFormatter.ofPattern("MMM 'at deze tijd broederrr' yyyy h:mm:ss:nn");
        System.out.println(yz.format(formatter));

        /*
        Je kan formatter en datetime met elkaar omwisselen
         */
        LocalDate date = LocalDate.of(2022, 05, 24);
        LocalTime time = LocalTime.of(20, 15);
        LocalDateTime localDateTime = LocalDateTime.of(date, time);
        DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("yyyy MMM dd ':' hh:mm 'Dit is de tijd'");
        System.out.println(localDateTime.format(formatter1));
        System.out.println(formatter1.format(localDateTime));
        System.out.println(formatter1.format(localDateTime) + "EXTRA");
    }
}

package Multithreading;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TransformParallelStream {
    public static void main(String[] args) {

        /**
         * Hier maak je van een Collection object een paralelle stream van
         */
        Stream<Integer> list = List.of(1,2).parallelStream();

        /**
         * Hier maak je van een Stream een parallele stream van.
         */
        Stream<Integer> list2 = Stream.of(1, 2).parallel();

//        List<Integer> list3 =
//        list.map(x -> x+2).collect(Collectors.toList());
//        System.out.println(list3);
//
//        long start = System.currentTimeMillis();
//        List.of(1,2,3,4,5)
//                .stream()
//                .map(e -> doWork(e))
//                .forEach(System.out::println);
//
//        System.out.println();
//        var timeTaken = (System.currentTimeMillis() - start)/1000;
//        System.out.println("Time: " + timeTaken + " seconds");

        long start2 = System.currentTimeMillis();
        List.of(1,2,3,4,5)
                .stream()
                .parallel()
                .map(e -> doWork(e))
                .forEachOrdered(System.out::println);

        System.out.println();
        var timeTaken2 = (System.currentTimeMillis() - start2)/1000;
        System.out.println("Time: " + timeTaken2 + " seconds");
        System.out.println();
        System.out.println("find first: " + List.of(1,2,3,4,5,6,7,8)
                .parallelStream()
                .unordered()
                .skip(5)
                .limit(2)
                .findFirst()
                .get());
        System.out.println();
        System.out.println("reduce: " + List.of(1,2,3,4,5,6,7,8)
                .parallelStream()
                .reduce(0,
                        (x,y) -> x + y,
                        (a,b) -> a + b));

        System.out.println(List.of('w', 'o', 'l', 'f')
                                .stream()
                                .reduce("",
                                        (a,b) -> a+b, //alle characters op basis van int waarde bij elkaar optellen (w + o = wo, l+f = lf)
                                        (x,y) -> x+y // alle opgetelde characters in character optellen (wo + lf = wolf)
                                        ));

        System.out.println(List.of('w', 'o', 'l', 'f')
                            .stream()
                            .reduce("X",
                                    (a,b) -> a + b,
                                    (x,y) -> x + y
                            ));

        Stream<String> stream1 = Stream.of("w", "o", "l", "f");
        SortedSet<String> set = stream1
                            .parallel()
                            .collect(ConcurrentSkipListSet::new,
                                    Set::add,
                                    Set::addAll
                            );
        System.out.println(set);
        /**
         * Belangrijke voorwaarden om collect parallel te gebruiken
         * stream parallel
         * Collect moet characteristic CONCURRENT hebben
         * Collectors moet characteristic UNORDERED hebben
         *
         * Een set is niet UNORDERED, dus daar zal geen parallel collect reduction op uitgevoerd worden
         */
        Set<String> stream2 = List.of("String1", "String2")
                                    .parallelStream()
                                    .collect(Collectors.toSet());
        System.out.println("Stream 2: " + stream2);

        Stream<String> stream3 = List.of("Lion", "Bear", "Giraffe", "Monkey").parallelStream();
        ConcurrentMap<Integer, String> concurrentHashMap = stream3
                                                            .collect(Collectors.toConcurrentMap(String::length, //Een Integer key wordt gevormd
                                                                    k -> k, //De waarde die bij de bovenstaande key hoort
                                                                    (s1, s2) -> s1 + "," + s2)); //die merger is voor als een key
                                                                                                 // al een value heeft. Dan wordt in dit

        Stream<String> stream4 = List.of("Lion", "Bear", "Giraffe", "Monkey").parallelStream();
        ConcurrentMap<Integer, List<String>> groupingByConcurrent = stream4
                .collect(Collectors.groupingByConcurrent(String::length));
        System.out.println(groupingByConcurrent);

    }


    private static int doWork(int input) {
        try {
            Thread.sleep(100);
        } catch(InterruptedException e) {
            return input;
        }
        return input;
    }
}

package Multithreading;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class LockMethode {
    private static int x = 0;
    private final ReentrantLock lock = new ReentrantLock();
    public static void main(String[] args) throws InterruptedException {
        LockMethode lm = new LockMethode();
        lm.LockMethod();
    }
    public static synchronized void increment() {
        System.out.println(++x);
    }

    public void LockMethod() {
        ExecutorService executorService = null;

        if (lock.tryLock()) {
            try {
                lock.lock();
                lock.lock();
                executorService = Executors.newFixedThreadPool(10);
                for (int i = 0; i < 10; i++) {
                    executorService.submit(LockMethode::increment);
                }
            } finally {
                if (executorService != null) executorService.shutdown();
                /**
                 * 3x lock, dus dan ook 3x unlock.
                 */
                lock.unlock();
                lock.unlock();
                lock.unlock();
            }
        } else {
            System.out.println("Lock already given away");
        }
        /**
         * Geen genoeg unlocks? Dan geeft tryLock een false terug
         */
        new Thread(() -> System.out.println(lock.tryLock())).start();
    }

}

package Multithreading;

import java.lang.reflect.Array;
import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.*;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class ParallelStream {
    /**
     * Avoid stateful operations inside a stream
     */

    public static void main(String[] args) {
        IntStream intStream = IntStream.range(1, 11);
        ParallelStream ps = new ParallelStream();
        ps.addValue(intStream.parallel());

        Integer i1 = List.of(1,2,3,4).stream().findAny().get();
        synchronized (i1) {
            Integer i2 = List.of(1,2,3,4)
                    .parallelStream()
                    .sorted()
                    .findAny()
                    .get();
            System.out.println(i2);
        }

        var x =
        List.of("Duck", "Pikachu", "Dolphin")
                .parallelStream()
                .mapToInt(String::length)
                .boxed()
                .reduce(
                        0,
                        (s1,s2) -> s1 + s2,
                        (x1, x2) -> x1 + x2);
        System.out.println("Total length: " + x);

        var s = Executors.newScheduledThreadPool(10);
        DoubleStream.of(3.112, 2.743)
                .forEach(c -> s.submit(
                        () -> System.out.println(10*c)
                ));
        s.execute(() -> System.out.println("Printed"));
        s.shutdown();
        s.schedule(() -> System.out.println("Hoi"), 10, TimeUnit.SECONDS);

    }

    public void addValue(IntStream source) {
        var x = Collections.synchronizedList(new ArrayList<Integer>());
//        source
//                .filter(a -> a % 2 ==0)
//                .forEach(i -> x.add(i));
//        System.out.println(x);

        List<Integer> list =
        source
                .boxed()
                .filter(a -> a % 2 ==0)
                .collect(Collectors.toList());
        System.out.println(x);
        System.out.println(list);
    }


}

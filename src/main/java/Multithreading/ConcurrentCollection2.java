package Multithreading;

import java.util.Collections;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ConcurrentCollection2 {
    public static void main(String[] args) {
        var foodData = new HashMap<String, Object>();
        ExecutorService es = Executors.newFixedThreadPool(10);
        foodData.put("penguin", 1);
        foodData.put("flamingo", 2);
        var synFoodData = Collections.synchronizedMap(foodData);
        /**
         * Exception, want de iterator wordt aangepast met een single thread. Je moet
         * dit dus wel met meerdere threads aanpassen, met een ExecutorService.
         */
        /*
        for(String key: synFoodData.keySet())
                synFoodData.remove(key); // Geeft compilatie error aan, want is met een single thread
         */
        try {
            for(String key: synFoodData.keySet())
                es.submit(() -> synFoodData.remove(key));
        } finally {
            es.shutdown();
        }

        System.out.println(synFoodData);
    }
}

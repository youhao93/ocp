package Multithreading;


import java.lang.reflect.Array;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;

public class ExecutorServiceExample {
    public static void main(String[] args) throws Exception {
        ExecutorService es = null;
        Runnable task1 = () -> System.out.println("First task");
        Runnable task2 = () -> System.out.println("Second task");

        /**
         * Runnable return type is void. Gebruik Callable om wel een
         * waarde terug te krijgen. Return type V in Callable functional interface.
         * Het roept vervolgens de call() method aan
         */
        Callable<String> c1 = () -> "Callable String";

        try {
            es = Executors.newSingleThreadExecutor();
            System.out.println("Beginning");
            /**
             * execute() method is 'fire and forget'. Het is een void return type.
             * Het resultaat is dus niet beschikbaar in de thread
             */
            es.execute(task1);
            es.execute(task2);
            es.execute(task1);
            /**
             * sumbit() method is zelfde als execute method, alleen geeft het
             * een Future object terug die aangeeft of de task klaar is.
             * Het accepteert ook een Generic type object, om het resultaat
             * op te vangen.
             */
            Future<?> f = es.submit(task1);
            AtomicInteger counter = new AtomicInteger();
            Future<?> f2 = es.submit(() -> {
                for (int i = 0; i < 5; i++) {
                    counter.getAndIncrement();
                }
            });
            Future<String> f3 = es.submit(c1);
            Future<Integer> f4 = es.submit(() -> 30 + 15
            );
            f4.cancel(true);
            System.out.println("Is f1 het klaar? " + f.isDone());
            System.out.println("Is f2 het klaar? " + f2.isDone());
            System.out.println("Is f3 het klaar? " + f3.isDone());
            System.out.println("End");
            Thread.sleep(50);
            System.out.println("Is f1 het klaar? " + f.isDone());
            System.out.println("Is f2 het klaar? " + f.isDone());
            System.out.println("Wat is het resultaat: " + f.get());
            System.out.println("Wat is het resultaat: " + f2.get(10, TimeUnit.SECONDS));
            System.out.println("Wat is het resultaat: " + f3.get(10, TimeUnit.SECONDS));
            System.out.println("f4: " + f4.get());
        } finally {
            if(es!=null) es.shutdown();
        }
        /**
         * ExecutorService weigert nieuwe taken na een shutdown() aanroep
         * ExecutorService kan nog de huidige taken aan het afronden zijn.
         * Pas als er geen tasks meer runt, dan is het 'terminated'
         */
        if(es!=null) {
            if(es.awaitTermination(1, TimeUnit.MINUTES)) {
                if(es.isTerminated()) System.out.println("Finished");
                else System.out.println("At least one task is running");
            }

        }

    }


    private class PrintData implements Runnable {
        @Override
        public void run() {
            for(int i = 0; i < 2; i++) {
                System.out.println("Multithreading.PrintData " + i);
            }
        }
    }
}



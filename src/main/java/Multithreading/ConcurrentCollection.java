package Multithreading;

import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentNavigableMap;

public class ConcurrentCollection {
    public static void main(String[] args) {
        Map<String, Integer> hashMap = new ConcurrentHashMap<String, Integer>();
        hashMap.put("Flamingo", 1);
        hashMap.put("Lion", 2);
        for(String key:hashMap.keySet()) {
            hashMap.put(key, 4);
        }
        System.out.println(hashMap);

        Queue<Integer> queue = new ConcurrentLinkedQueue<>();
        queue.offer(5);
        queue.offer(10);
        for(int i = 10; i<20;i++) {
            queue.offer(i);
        }
        System.out.println(queue.peek());
    }
}

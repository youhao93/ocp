package Multithreading;

import java.util.concurrent.*;

public class CyclicBarrierExample {
    public static void main(String[] args) {
        CyclicBarrierExample cbe = new CyclicBarrierExample();
        ExecutorService es = Executors.newFixedThreadPool(5);
        CyclicBarrier c1 = new CyclicBarrier(5, () -> System.out.println("Passed lions"));
        CyclicBarrier c2 = new CyclicBarrier(5, () -> System.out.println("Passed cleaning cages"));
        try {
            for(int i=0;i<5;i++) {
                es.submit(() -> cbe.performTask(c1, c2));
            }
        } finally {
            es.shutdown();
        }
    }


    public void performTask(CyclicBarrier c1, CyclicBarrier c2) {
        try {
            removeLions();
            c1.await();
            cleanCage();
            c2.await();
            addLions();
        } catch(InterruptedException | BrokenBarrierException e) {
            e.printStackTrace();
        }

    }

    private void removeLions() {
        System.out.println("Removing lions");
    }

    private void cleanCage() {
        System.out.println("Cleaning the cage");
    }

    private void addLions() {
        System.out.println("Adding lions");
    }

}

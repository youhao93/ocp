package d3;


import java.time.LocalDate;
import java.util.Enumeration;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.function.Predicate;

// Filename: TestClass.java
public class OuterWorld {
    private double d = 10.0;

    abstract class InnerPeace {
        public void calculate() {
            OuterWorld.this.d = 100.0;
        }
    }

    public static void main(String args[]) {
        Float f1 = 10.0f;
        Float f2 = 0.0f;
        Float f3 = null;
        double f = 0.0;
        try {
            f = f1 / f2;
            System.out.println(f);
            f3 = f1 / f2;
        } catch (Exception e) {
            System.out.println("Exception");
        }
        System.out.println(f3.isInfinite());


        Locale myloc = new Locale.Builder().setLanguage("en").setRegion("UK").build(); //L1
        ResourceBundle msgs = ResourceBundle.getBundle("mymsgs", myloc);
        Enumeration<String> en = msgs.getKeys();
        while (en.hasMoreElements()) {
            String key = en.nextElement();
            String val = msgs.getString(key);
            System.out.println(key + " : " + val);
        }

    }
}

class A {
    int max(int x, int y) {
        if (x > y) return x;
        else return y;
    }
}

class B extends A {
    int max(int x, int y) {
        return 2 * super.max(x, y);
    }
}

class C extends B {

    class E {

    }

    ;

    int max(int x, int y) {
        return super.max(2 * x, 2 * y);
    }
}

interface testInterFace {
    private static void hallo() {
    }

    ;

    private static void buz() {
        System.out.println("Hallo");
    }

    private void buz2() {
    }


}
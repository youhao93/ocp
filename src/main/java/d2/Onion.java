package d2;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

interface Eatable {
    int types = 10;
}

class Food implements Eatable {
    public static int types = 20;
}
public class Onion {
    private String data = "skin";

    private class Layer extends Onion {
        String data = "thegoodpart";

        public String getData() {
            return data;
        }
    }

    public String getData() {
        return new Layer().getData();
    }



    public class DatabaseWrapper extends Food implements Eatable{
        String url = "jdbc://derby://localhost:1527//mydb";

        DatabaseWrapper getDatabase() {
            System.out.println("Getting DB");
            return null;
        }

    }

        public static void main(String[] args) throws Exception {
            Integer dataWrapper = 5;
            Integer value = 15;
            System.out.println(dataWrapper + value);
            var o = new Onion();
            System.out.println(o.getData());

            char[] a = {'h', 'e', 'l', 'l'};
            char[] b = {'a', 'e'};
            int x = Arrays.compare(a, b);
            int y = Arrays.mismatch(a, b);
            System.out.println(x + " " + y);
            try (var bfr = new BufferedReader(new InputStreamReader(System.in))) {
                System.out.println("Enter Number:");
//            var s = bfr.readLine();
//            System.out.println("Your Number is : " + s);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


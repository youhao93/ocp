package d2;


import java.io.Closeable;
import java.sql.PreparedStatement;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Executors;
import java.util.stream.Stream;

class Outsider {
    public class Insider {
    }
}

enum Coffee {
    ESPRESSO("Very Strong"), MOCHA("Bold"), LATTE("Mild");
    public String strength;

    Coffee(String strength) {
        this.strength = strength;
    }

    public String toString() {
        return strength;
    }
}

public class TestClass {
    char c;
    long l1;
    public int TestClass(long pLong) { l1 = pLong ; return 0;}  //(1)
    public static void main(String[] args) {
        TestClass a, b ;
        a = new TestClass();  //(2)
        //b = new TestClass(5);  //(3)

        TestClass tc = new TestClass();
        System.out.println("Char value = " +  (int)tc.c);
        List.of(Coffee.values()).stream().forEach(e->{System.out.print(e.name()+":"+e+", ");});

        Optional<Object> optional = Optional.empty();

        var os = new Outsider();
        // 1 insert line here
        Outsider.Insider insider = os.new Insider();
    }
}





class Outer {
    private int Outer() {
        return 0;
    }

    protected class Inner {
    }
}


interface Processor {
    Iterable process();
}

interface ItemProcessor extends Processor {
    Collection process();
}

interface WordProcessor extends Processor {
    Iterable process();

}

interface GenericProcessor extends ItemProcessor, WordProcessor {
}
package d2;

import d1.Moveable;

public class Donkey implements Moveable{

    int location = 200;
    public void move(int by) {
        location = location + by;
    }

    public void moveBack(int by) {
        location = location - by;
    }
}

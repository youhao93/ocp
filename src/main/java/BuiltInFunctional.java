import java.time.LocalDate;
import java.util.HashMap;
import java.util.Optional;
import java.util.function.*;

public class BuiltInFunctional {

    Supplier<LocalDate> s1 = () -> LocalDate.now();
    Supplier<LocalDate> s2 = LocalDate::now;

    Function<String, Integer> f1 = String::length;

    BiFunction<String, String, String> biF1 = String::concat;
    BiFunction<String, String, String> biF2 = (x, y) -> x.concat(y);

    HashMap<Integer, String> map = new HashMap<>();
    BiConsumer<Integer, String> biC1 = map::put;
    BiConsumer<Integer, String> biC2 = (x, y) -> map.put(x, y);

    Predicate<String> p1 = String::isEmpty;
    BiPredicate<String, String> p2 = String::startsWith;

    Predicate<String> eggs = s -> s.contains("egg");
    Predicate<String> brown = s -> s.contains("brown");
    Predicate<String> otherEggs = s -> s.contains("egg") && !s.contains("brown");

    //kortere manier
    Predicate<String> brownEggs1 = eggs.and(brown);
    Predicate<String> brownEggsNegate = eggs.and(brown.negate());

    Consumer<String> c1 = x -> System.out.println("1 " + x);
    Consumer<String> c2 = x -> System.out.println("2 " + x);
    Consumer<String> combined = c1.andThen(c2);


    public static void main(String[] args) {
        BuiltInFunctional b1 = new BuiltInFunctional();
        System.out.println(b1.s1.get());
        System.out.println(b1.s2.get());

        b1.biC1.accept(1, "Eerste waarde");
        b1.biC2.accept(2, "Tweede waarde");
        System.out.println(b1.map);;

        System.out.println(b1.f1.apply("Chick"));
        System.out.println(b1.f1.apply("Dit is een hele lange string"));

        System.out.println(b1.biF1.apply("Chick", "Baby"));
        System.out.println(b1.biF2.apply("Chipotle", "Smart"));

        System.out.println(b1.p1.test(""));
        System.out.println(b1.p1.test("Waarde"));
        System.out.println(b1.p2.test("Waarde", "Wa"));

        TriFunction<String, String, String, String> triFunction1 = (x, y, z) -> BuiltInFunctional.trippleConcat(x, y, z);
        TriFunction<String, String, String, String> triFunction2 = BuiltInFunctional::trippleConcat;
        System.out.println(triFunction1.apply("Test1", "Test2", "Test3"));
        System.out.println(triFunction2.apply("Test1", "Test2", "Test3"));

        b1.combined.accept("Annie");

        System.out.println(average(10, 20, 30));
        System.out.println(average());

        Optional<Double> optional = average(10, 20, 10);
        if(optional.isPresent()) System.out.println(optional.get());
        // single expression functional style
        optional.ifPresent(System.out::println);

    }

    public static String trippleConcat(String s1, String s2, String s3) {
        return s1.concat(s2).concat(s3);
    }

    public static Optional<Double> average(int... scores) {
        if(scores.length == 0) return Optional.empty();
        int sum = 0;
        for(int score : scores) sum += score;
        return Optional.of((double) sum / scores.length);
    }

}



package OverridingMethods;

import java.util.*;

public class OverridingMethods1 {
}


class Base {
    public <T extends CharSequence> Collection<String> transform(Collection<T> list) {
        return new ArrayList<String>();
    }
}

class Derived extends Base {
//    public  Collection<String> transform(Collection<String> list) { return new HashSet<String>(); }; //1
// public <T extends String> Collection<T> transform(Collection<T> list) { return new HashSet<T>();}; //2
//    public <T extends CharSequence> List<T> transform(Collection<T> list) {return new ArrayList<T>();}; //3
// public <T extends CharSequence> Collection<T> transform(List<T> list) { return new HashSet<T>(); }; //4
// public <T super String> Collection<T> transform(List<String> list) { return new HashSet<T>(); };//5
// public  Collection<CharSequence> transform(Collection<CharSequence> list) { return new HashSet<CharSequence>();}; //6
//public <T extends CharSequence> List<T> transform(Collection<T> list) { return new ArrayList<T>(); }; //3

    void crazyLoop() {
        var c = 0;
        JACK:
        while (c < 8) {
            JILL:
            for (int i = 0; i < 10; i++) {
                System.out.println(c);
            }
//            if (c > 3) break JILL;
//            else c++;


        }
    }
}

interface Device {
    public abstract void switchOn();
}

abstract class Router /* LOCATION 1 */ {    /* LOCATION 2 */
    public void switchOn() {
    }

    public abstract void reset();
}

class MimoRouter extends Router implements Device {    /* LOCATION 3 */
    public void reset() {
        System.out.println("Hallo");
    }

    public static void main(String[] args) {

        int[] b = new int[5];
        MimoRouter[] mimoRouters = {new MimoRouter(), new MimoRouter(), new MimoRouter()};


        Collection<Number> col = new HashSet<>();
        col.add(1);
        var list1 = List.of(col); //1

        int[][] a = new int[2][];
        a[0] = new int[2];
        a[1] = new int[4];
        a[0][0] = 1;
        a[0][1] = 2;
        a[1][0] = 3;
        a[1][1] = 4;
        a[1][2] = 5;
        a[1][3] = 6;
        MimoRouter mr = new MimoRouter();
        mr.method(null);

        int i = 0;
        for(i = 0;;) System.out.println("Hallo " + i);
    }

    public void method(Object o) {
        System.out.println("Object Version");
    }

    public void method(java.io.FileNotFoundException s) {
        System.out.println("java.io.FileNotFoundException Version");
    }

    public void method(java.io.IOException s) {
        System.out.println("IOException Version");
    }
}


interface House {
    public default String getAddress() {
        return "101 Main Str House";
    }
}

interface Bungalow extends House {
    public default String getAddress() {
        return "101 Smart Str Bungalow";
    }
}

class MyHouse implements Bungalow, House {
}

/**
 * Onthouden. Als een I2 extends van I2 en class c1 implementert zowel I2 als I2. En I1 heeft
 * een abstracte methode, maar als I2 die methode al override, hoef je de methode niet nog een keer
 * in c1 te overriden, dan wordt vanzelf de geimplementeerde methode van I2 genomen.
 * <p>
 * Als zowel I1 als I2 default methodes hebben, dan hoef je in c1 nog steeds geen methode te overriden EN
 * je hoeft ook geen specifieke implementatie te geven, dat iets ambigieus is. Dat is WEL het geval als
 * I2 NIET extends van I1. Dus let op of interfaces van elkaar extenden, zo ja, dan minder risico. Zo niet
 * en op gelijke niveau? Dan gevaar.
 */
class TestClass {
    public static void main(String[] args) {
        House ci = new MyHouse();  //1
        System.out.println(ci.getAddress()); //2
    }
}
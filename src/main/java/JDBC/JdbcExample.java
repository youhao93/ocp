package JDBC;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class JdbcExample {

    public static void main(String[] args) {
        String url="jdbc:mysql://localhost:3306/java";
        String username="root";
        String password="";
        try(Connection con = DriverManager.getConnection(url, username, password);
            Statement statement = con.createStatement()){
            /**
             * execute(), executeUpdate(), executeQuery()
             * executeUpdate -> insert, update and delete
             */
            statement.executeUpdate("INSERT INTO product values(NULL, 'beer', 5)"); //send a query to DMBS
            System.out.println("Connection succesfull");

        } catch(SQLException e) {
            e.printStackTrace();
        }
    }




}

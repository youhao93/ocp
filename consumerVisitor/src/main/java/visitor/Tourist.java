package visitor;

import api.Tour;
import reservation.TourFinder;

import java.util.List;

/**
 * Next up is to call the service locator by a consumer.
 * A consumer (or client) refers to a module that obtains and uses a service.
 * Once the consumer has acquired a service via the service locator,
 * it is able to invoke the methods provided by the service provider interface.
 *
 */
public class Tourist {
    public static void main(String[] args) {
        Tour tour = TourFinder.findTour();
        assert tour != null;
        System.out.println("Single tour " + tour.name());

        List<Tour> tours = TourFinder.findAllTours();
        System.out.println("All tours " + tours.size());
    }

}

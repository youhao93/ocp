module serviceProviderAgency {
    requires serviceProviderInterfaceApi;
    /**
     * provides interfaceName with className;
     * Alleen de implementatie is beschikbaar
     * voor de service provider door gebruik te maken
     * van de interface
     */
    provides api.Tour with agency.TourImpl;
}
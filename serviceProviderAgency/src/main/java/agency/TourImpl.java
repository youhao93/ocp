package agency;

import api.Souvenir;
import api.Tour;

/**
 * Service provider
 */
public class TourImpl implements Tour {

    @Override
    public String name() {
        return "angel";
    }

    @Override
    public int length() {
        return 120;
    }

    @Override
    public Souvenir getSouvenir() {
        Souvenir souvenir = new Souvenir();
        souvenir.setDescription("Dit is een souvenir");
        return souvenir;
    }
}
